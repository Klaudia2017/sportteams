import React, { Component } from 'react';
import axios from 'axios';

export default class TeamDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            team: [],
        };
        this.getTeamDetails = this.getTeamDetails.bind(this);
    }

    componentWillMount = () => {
      this.getTeamDetails(this.props.teamId);
    };
    
    getTeamDetails(id) {
        let url = `https://www.thesportsdb.com/api/v1/json/1/lookupteam.php?id=${id}`;
        axios.get(url).then((response) => {
            let data = response.data.teams[0];
            this.setState({
                team: data,
            });
        })
        .catch((error) => {
            if (error.response) {
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            } else {
                console.log('Error', error.message);
            }
        });

    }

    render() {
        const { team } = this.state;
        return (
        <div>
            { this.props.teamId && (
                <div className="card mb-2"> 
                    <img src={team.strTeamBanner} alt="banner" className="card-img-top"/>
                    <div className="card-body">
                        <h4>{team.strTeam} - {team.strStadiumLocation}</h4>
                        <p className="card-text">{team.strDescriptionEN}</p>
                    </div>
                </div>
            )}
        </div>
        )
    }
}
