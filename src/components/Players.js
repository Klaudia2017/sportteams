import React, { Component } from 'react'
import axios from 'axios';

export default class Players extends Component {
    constructor() {
        super()
        this.state = {
            players: [],
            currentPlayer: {strPlayer: '', idPlayer: ''},
        };
        this.handleInput = this.handleInput.bind(this);
        this.addItem = this.addItem.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
    }

    componentDidMount() {
        this.getPlayers();
        this.updateBreadcrumb();
    }

    updateBreadcrumb = () => {
        const breadcrumbData = [
            {
              title: `Clubs`,
              link: '/'
            },
            {
              title: `Players`,
              link: '/' + this.props.match.params.id,
            }
        ];
        this.props.updateBreadcrumb(breadcrumbData);
    };

    getPlayers() {
        let url = `https://www.thesportsdb.com/api/v1/json/1/lookup_all_players.php?id=${this.props.match.params.id}`;
        axios.get(url).then((response) => {
           let data = response.data.player;
           this.setState({
               players: data,
            })
        })
        .catch((error) => {
            if (error.response) {
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            } else {
                console.log('Error', error.message);
            }
        });
    }

    handleInput(event) {
        this.setState({value: event.target.value});
        const inputText = event.target.value;
        const currentPlayer = { strPlayer: inputText, idPlayer: Date.now() }
        this.setState({
            currentPlayer,
        })
    }

    addItem(event) {
        event.preventDefault();
        const newItem = this.state.currentPlayer;
        if (newItem.strPlayer !== '') {
          let newPlayers = this.state.players;
          newPlayers.unshift(newItem);
          this.setState({
            players: newPlayers,
            currentPlayer: { strPlayer: '', idPlayer: '' },
           });
           this.forceUpdate();
        }
    }

    deleteItem(idPlayer) {
        const filteredPlayers = this.state.players.filter(item => {
            return item.idPlayer !== idPlayer
        });
        this.setState({
            players: filteredPlayers,
        });
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    render() {
        const { players, currentPlayer : {strPlayer} } = this.state;
        return (
        <div>
            <form onSubmit={this.addItem} className="row mb-4">
                <div className="col-12 col-lg-8">
                    <input
                    type="text"
                    value={strPlayer}
                    onChange={this.handleInput}
                    className="form-control"
                    />
                </div>
                <div className="col-12 col-lg-2 mt-4 m-lg-0">
                    <button type="submit"className="btn btn-info btn-block">add to list</button>
                </div>
            </form>

            <div className="row mb-4">
                <ul className="col-12 col-lg-10">
                    {players.map(item => (
                        <li 
                        key={item.idPlayer}
                        className="border rounded p-2 mb-2 d-flex justify-content-between align-items-center"
                        >
                            <span className="pl-2">
                                {item.strPlayer}
                            </span>
                            <button 
                            type="text"
                            className="btn btn-outline-info btn-sm"
                            onClick={() => this.deleteItem(item.idPlayer)}
                            >delete</button>
                        </li>
                    ))}
                </ul>
            </div>
        </div>
        )
    }
    
}
