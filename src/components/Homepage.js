import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Breadcrumb from 'components/Breadcrumb';
import Teams from 'components/Teams';
import Players from 'components/Players';

export default class Homepage extends Component {
    constructor(props) {
        super(props);
        this.state = {
          breadcrumbData: []
        }
    }

    updateBreadcrumb = newBreadcrumbData => {
        this.setState({ breadcrumbData: newBreadcrumbData });
    };

    render() {
        const { breadcrumbData } = this.state;
        return (
            <Router>   
                <div>
                    <div className="navbar navbar-light bg-info">
                        <h1 className="text-white">English Football League</h1>
                    </div>
                    {breadcrumbData && <Breadcrumb data={breadcrumbData}/>}
                    <div className="m-4">
                        <Switch>
                            <Route path='/' 
                            exact={true}
                            render={() => (
                                <Teams
                                  updateBreadcrumb={this.updateBreadcrumb}
                                />
                            )}
                            />
                            <Route 
                            path='/:id' 
                            render={(props) => (
                                <Players
                                  updateBreadcrumb={this.updateBreadcrumb}
                                  {...props}
                                />
                            )}
                            />
                        </Switch>
                    </div>
                </div>
            </Router>
        )
    }
}