import React, { Component } from 'react';
import TeamDetail from 'components/TeamDetail';
import { Link } from 'react-router-dom';
import axios from 'axios';

export default class Teams extends Component {
    constructor() {
        super()
        this.state = {
            teams: [],
            teamId: null,
        };
        this.SeeTeamId = this.SeeTeamId.bind(this);
    }

    componentDidMount() {
        this.getPlayers();
        this.updateBreadcrumb();
    }

    updateBreadcrumb = () => {
        const breadcrumbData = [
            {
              title: `Clubs`,
              link: '/'
            }
        ];
        this.props.updateBreadcrumb(breadcrumbData);
    };

    getPlayers() {
        let url = `https://www.thesportsdb.com/api/v1/json/1/search_all_teams.php?l=English%20Premier%20League`;
        axios.get(url).then((response) => {
           let data = response.data.teams;
           this.setState({
                teams: data,
            })
        }).catch((error) => {
            if (error.response) {
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            } else {
                console.log('Error', error.message);
            }
        });
    }

    SeeTeamId(id) {
        if(id !== this.state.teamId) {
            this.setState({ teamId: id });
        } else {
            this.setState({ teamId: null });
        }
    }

    render() {
        const { teams, teamId } = this.state;
        return (
        <div className="mr-2">
            <div className="row mb-4">
                <ul className="col-12 col-lg-10">
                    {teams.map(item => (
                        <div key={item.idTeam}>
                            <li 
                            className="border rounded p-2 mb-2 d-flex flex-column justify-content-start align-items-start flex-md-row justify-content-md-between align-items-md-center"
                            >
                                <span className="pl-2 mb-3 mb-md-0">
                                    {item.strTeam}
                                </span>
                                <div>
                                    <Link
                                    to={item.idTeam}
                                    title="See list"
                                    params={{ id: item.idTeam }}
                                    >
                                        <button 
                                        type="button"
                                        className="btn btn-outline-info mr-2 btn-sm "
                                        >
                                        See players
                                        </button>
                                    </Link>
                                    <button 
                                    type="button"
                                    onClick={() => this.SeeTeamId(item.idTeam)}
                                    className="btn btn-outline-info mr-2 btn-sm"
                                    >
                                        {item.idTeam !== teamId ? <span>See details</span> : <span>Close</span>}
                                    </button>
                                </div>
                            </li>
                            {item.idTeam === teamId && <TeamDetail teamId={teamId}/>}
                        </div>
                    ))}
                </ul>
            </div>
        </div>
        )
    }
}
