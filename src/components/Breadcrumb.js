import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Breadcrumb extends Component {
  constructor(props) {
    super(props);
    this.state = {
        breadcrumb: []
    };
  }

  componentWillReceiveProps = (nextProps) => {
    if(nextProps.data !== this.state.props) {
        this.setState({ breadcrumb: nextProps.data });
    }
  }
  
  render() {
      const { breadcrumb } = this.state;
    return (
      <nav
      >
        <ol className="breadcrumb pl-4">
            {breadcrumb &&
                breadcrumb.map((object, i) => (
                    <li key={i} className="breadcrumb-item">
                        <Link to={object.link} >
                            {object.title}
                        </Link>
                    </li>
                )
            )}
        </ol>
      </nav>
    );
  }
}

export default Breadcrumb;
/*

.btn-outline-info:focus, .btn-outline-info.focus {
    box-shadow: 0 0 0 0.2rem rgba(23, 162, 184, 0.5);
}
*/